//
//  CollectionViewController.swift
//  MemeMeV1.1
//
//  Created by John Peppe on 23.06.2021.
//

import Foundation
import UIKit

class CollectionViewController {
    var memes: [Meme]! {
        let object = UIApplication.shared.delegate
        let appDelegate = object as! AppDelegate
        return appDelegate.memes
    }
}
