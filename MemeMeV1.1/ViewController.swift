//
//  ViewController.swift
//  MemeMeV1.1
//
//  Created by John Peppe on 09.06.2021.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    //MARK: IBOutlets and Text Arributes
    @IBOutlet weak var headerNav: UIToolbar!
    @IBOutlet weak var footerNav: UIToolbar!
    @IBOutlet weak var pickedImageView: UIImageView!
    @IBOutlet weak var topTextField: UITextField!
    @IBOutlet weak var bottomTextField: UITextField!
    @IBOutlet weak var shareButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var cameraButton: UIBarButtonItem!
    @IBOutlet weak var albumButton: UIBarButtonItem!
    
    let memeMeTextAttributes: [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.backgroundColor: UIColor.clear,
        NSAttributedString.Key.strokeColor: UIColor.black,
        NSAttributedString.Key.foregroundColor: UIColor.white,
        NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-CondensedBlack", size: 40)!,
        NSAttributedString.Key.strokeWidth: -5
    ]
    
    //MARK: Screen Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialUIState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cameraButton.isEnabled = UIImagePickerController.isSourceTypeAvailable(.camera)
        subscribeToKeyboardNotification()
        memeTextView(textFields: [topTextField, bottomTextField])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeFromKeyboardNotifications()
    }
    
    func memeTextView(textFields: [UITextField]) {
        for text in textFields {
            text.defaultTextAttributes = memeMeTextAttributes
            text.delegate = self
            text.textAlignment = .center
        }
    }
    
    //MARK: Toolbar Features

    @IBAction func share(_ sender: UIBarButtonItem) {
        
        let items = [generateMeme()]
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)

        present(activityViewController, animated: true)
        activityViewController.completionWithItemsHandler = { (activityType: UIActivity.ActivityType?, complete:
        Bool, arrayReturnedItems: [Any]?, error: Error?) in
            if complete {
                print("saved")
                self.save()
            }
            else {
              print("not saved")
            }
        }
        
    }

    @IBAction func cancelMeme(_ sender: UIBarButtonItem) {
        initialUIState()
    }
    
    //MARK: Select an image
    
    @IBAction func pickAlbumImage(_ sender: Any) {
        presentPickImageController(source: .photoLibrary)
    }
    
    @IBAction func pickCameraImage(_ sender: Any) {
        presentPickImageController(source: .camera)
    }
    
    func presentPickImageController(source: UIImagePickerController.SourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = source
        present(imagePicker, animated: true, completion: nil)
    }
    
    func  imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        shareButton.isEnabled = true
        cancelButton.isEnabled = true
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            pickedImageView.image = image
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerViewDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldBeginTyping(_ textField: UITextField) -> Bool {
        textField.clearsOnBeginEditing = true
        textField.defaultTextAttributes = memeMeTextAttributes
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    //MARK: Keyboard Control
    
    func subscribeToKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        if(bottomTextField.isEditing) {
            view.frame.origin.y -= getKeyboardHeight(notification)
        }
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        view.frame.origin.y = 0
    }

    func getKeyboardHeight(_ notification: Notification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        return keyboardSize.cgRectValue.height
    }

    func unsubscribeFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    //MARK: Meme Generator
    
    func generateMeme() -> UIImage {
        setToolBarState(hidden: true)
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        view.drawHierarchy(in: self.view.frame, afterScreenUpdates: true)
        let memedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        setToolBarState(hidden: false)
        
        return memedImage
    }
    
    func setToolBarState(hidden: Bool) {
        footerNav.isHidden = hidden
    }
    
    func save() {
        let meme = Meme(topText: topTextField.text!, bottomText: bottomTextField.text!, originalImage: pickedImageView.image!, memedImage: generateMeme())
        
        let object = UIApplication.shared.delegate
        let appDelegate = object as! AppDelegate
        appDelegate.memes.append(meme)
    }
    
    func initialUIState() {
        topTextField.text = "TOP"
        bottomTextField.text = "BOTTOM"
        pickedImageView.image = nil
        shareButton.isEnabled = false
        cancelButton.isEnabled = false
    }
    
}

