//
//  MemeStruct.swift
//  MemeMeV1.1
//
//  Created by John Peppe on 10.06.2021.
//

import Foundation
import UIKit

struct Meme {
    let topText: String?
    let bottomText: String?
    let originalImage: UIImage?
    let memedImage: UIImage?
    
}
